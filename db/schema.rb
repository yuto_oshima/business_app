# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_27_053611) do

  create_table "events", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title"
    t.string "address"
    t.datetime "start_datetime"
    t.datetime "end_datetime"
    t.string "url"
    t.bigint "website_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["website_id"], name: "index_events_on_website_id"
  end

  create_table "websites", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title"
    t.string "site_url"
    t.string "event_url"
    t.string "content_xpath"
    t.string "start_datetime_xpath"
    t.string "end_datetime_xpath"
    t.string "link_xpath"
    t.string "address_xpath"
    t.string "next_page_xpath"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title_xpath"
  end

  add_foreign_key "events", "websites"
end
