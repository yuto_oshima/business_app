class CreateWebsites < ActiveRecord::Migration[5.2]
  def change
    create_table :websites do |t|
      t.string :title
      t.string :site_url
      t.string :event_url
      t.string :content_xpath
      t.string :start_datetime_xpath
      t.string :end_datetime_xpath
      t.string :link_xpath
      t.string :address_xpath
      t.string :next_page_xpath

      t.timestamps
    end
  end
end
