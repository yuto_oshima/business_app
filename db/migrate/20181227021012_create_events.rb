class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :address
      t.datetime :start_datetime
      t.datetime :end_datetime
      t.string :url
      t.references :website, foreign_key: true

      t.timestamps
    end
  end
end
