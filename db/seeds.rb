# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

techplay = Website.create(
		title: "TECH PLAY",
		site_url: "https://techplay.jp",
		event_url: "https://techplay.jp/event/search?yet=1&sort=started_asc&start=",
		content_xpath: "//*[@id='main-content']/article/div[contains(@class, 'eventlist')]",
		start_datetime_xpath: ".//span[@itemprop='startDate']",
		end_datetime_xpath: ".//span[@itemprop='endDate']",
	  link_xpath: ".//h3/a",
	  address_xpath: "div[@class='eventlist-right']//div[@class='address']/span[1]",
    next_page_xpath: "//*[@id='main-content']/article/div[@class='pager ']/a[contains(text(), '>')]", 
	  created_at: "2018-12-27 05:47:21",
		updated_at: "2018-12-27 05:47:21",
		title_xpath: ".//h3/a/span"
)
atnd = Website.create(
	title: "atnd", 
	site_url: "https://atnd.org", 
	event_url: "https://atnd.org/events/search?q%5Btitle_or_description_or_place_or_address_cont%5D=&q%5Bplace_or_address_cont%5D=&q%5Bstarted_at_gteq_beginning_of_day%5D=", 
	content_xpath: ".//ul[@class='search-result-list']/li", 
	start_datetime_xpath: ".//div[@class='date']/span", 
	end_datetime_xpath: ".//div[@class='date']/span", 
	link_xpath: "a[@class='event']", 
	address_xpath: ".//div[@class='place']/span[2]", 
	next_page_xpath: ".//div[@class='pagination']/a[@class='next_page']", 
	created_at: "2018-12-27 06:11:59", updated_at: "2018-12-27 06:11:59", 
	title_xpath: ".//div[@class='title']"
)
doorkeeper = Website.create(
	title: "doorkeeper", 
	site_url: "https://www.doorkeeper.jp", 
	event_url: "https://www.doorkeeper.jp/events?since=", 
	content_xpath: ".//div[@class='global-event-list']/div[1]/div[@class='global-event-list-events-that-day']/div[@class='global-event events-list']", 
	start_datetime_xpath: ".//time[@itemprop='startDate']", 
	end_datetime_xpath: nil, 
	link_xpath: ".//h3/a", 
	address_xpath: ".//span[@itemprop='address']", 
	next_page_xpath: "//ul[@class='pagination']/li[@class='next']/a", 
	created_at: "2018-12-27 06:15:47", 
	updated_at: "2018-12-27 06:15:47", 
	title_xpath: ".//h3/a/span"
)
connpass = Website.create(
	title: "connpass", 
	site_url: "https://connpass.com", 
	event_url: "https://connpass.com/calendar/?ym=", 
	content_xpath: ".//table[@class='common_table']/tbody/tr", 
	start_datetime_xpath: ".//span[@class='tooltip']", 
	end_datetime_xpath: ".//span[@class='tooltip']", 
	link_xpath: ".//a", address_xpath: ".//span[@class='tooltip']", 
	next_page_xpath: nil, created_at: "2018-12-27 06:20:09", 
	updated_at: "2018-12-27 06:20:09", 
	title_xpath: ".//a"
)
