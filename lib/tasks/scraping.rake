require 'open-uri'
namespace :scraping do
	desc "Webサイトのスクレピング"
	task :scrape, ["date"] => :environment  do |task, args|
     args.with_defaults(date: 4)
    scraping_date = Date.today + args.date.to_i #取ってくる日付
		websites = Website.all #websiteの全てのオブジェクトを返して
    websites.each do |website| #全てのオブジェクト一つずつ回していく
      scraping(website, scraping_date)      
    end
	end

	def scraping(website, scraping_date)
    existing_urls = Event.where("start_datetime >= ? and start_datetime < ?", scraping_date, (scraping_date + 1)).pluck(:url)
    events = []
		case website.site_url
		when "https://techplay.jp"
			loop = true
			url = website.event_url + scraping_date.to_s
   		while loop do
   		  doc = open_url(url)
   		  #containsから返された複数あるeventlistを.eachで回す
   		  doc.xpath(website.content_xpath).each do |node|
   		    if node.xpath(".//div[@class='apilogo']/img").attribute("src").value == "https://techplay.jp/images/top/apilogo_dots.png" 
   		    	event = website.events.build #websiteから紐づけいたeventのインスタンスを作成
   		  	  #タイトルを取得
     		  	event.title = node.xpath(website.title_xpath).inner_text
     		  	# 各リンクを取得
     		  	event.url = node.xpath(website.link_xpath).attribute('href').value
     		  	# 日付、時間の取得
     		  	node.xpath(website.start_datetime_xpath).present? ? event.start_datetime = node.xpath(website.start_datetime_xpath).attribute("content").value : event.start_datetime = ""
     		  	node.xpath(website.end_datetime_xpath).present? ? event.end_datetime = node.xpath(website.end_datetime_xpath).attribute("content").value : event.end_datetime = ""
     		  	#存在していたらif直下を実行(場所を取得)
     		  	event.address = node.xpath(website.address_xpath).inner_text
     		  	events << event unless existing_urls.include?(event.url)
     		  end #ifのend   
   		  end #eachのend
   		  # 続きのページがあるか見る
   		  if  doc.xpath(website.next_page_xpath).present?
   			  url = doc.xpath(website.next_page_xpath).attribute('href').value
   			else
   				loop = false
   		  end
   		end #whileのend

   	when "https://atnd.org"
      loop = true
      url = website.event_url + scraping_date.to_s + "&q%5Bstarted_at_lteq_end_of_day%5D=" + scraping_date.to_s + "&q%5Battendee_gteq%5D=0&q%5Battendee_gteq%5D=1&q%5Bnot_full%5D=0"
      while loop do
     	  doc = open_url(url)
     	  #containsから返された複数あるeventlistを.eachで回す
     	  doc.xpath(website.content_xpath).each do |node| 
     	  	event = website.events.build
     	  	#タイトルを取得
     	  	event.title = node.xpath(website.title_xpath).inner_text      
     	  	# 各リンクを取得
     	  	event.url = "https://atnd.org" + node.xpath(website.link_xpath).attribute('href').value     
     	  	# 日付、時間の取得
     	  	datetime = node.xpath(website.start_datetime_xpath).inner_text
     	  	event.start_datetime = Time.parse(datetime[0..3]+datetime[5..6]+datetime[8..9]+datetime[15..16]+datetime[18..19]+"00")
	  		  if datetime.length > 29
     	    	event.end_datetime = Time.parse(datetime[23..26]+datetime[28..29]+datetime[31..32]+datetime[38..39]+datetime[41..42]+"00")
    		  elsif datetime.length > 23
    		  	event.end_datetime = Time.parse(datetime[0..3]+datetime[5..6]+datetime[8..9]+datetime[23..24]+datetime[26..27]+"00")
    		  else
    		  	event.end_datetime = "" 
    		  end 			  
     	  	#存在していたらif直下を実行(場所を取得)
     	  	event.address = node.xpath(website.address_xpath).inner_text
     	  	events << event unless existing_urls.include?(event.url)  
     	  end #eachのend   
     	  # 続きのページがあるか見る
     	  if  doc.xpath(website.next_page_xpath).present?
     		  url = "https://atnd.org" + doc.xpath(website.next_page_xpath).attribute('href').value #相対パスの場合、前にドメインを入れる
     		else
      		loop = false
     	  end
    	end #atndのwhileのend	

    when "https://www.doorkeeper.jp"
    	loop = true
    	list_day_xpath = ".//div[@class='global-event-list']/div[@class='global-event-list-day']"
    	url = website.event_url + CGI.escape(scraping_date.strftime("%Y/%m/%d"))
  		while loop do
  		  doc = open_url(url)
  		  #containsから返された複数あるeventlistを.eachで回す
  		  if Date.parse(doc.xpath(website.content_xpath)[0].xpath(website.start_datetime_xpath).attribute("datetime").value) == scraping_date
  		    doc.xpath(website.content_xpath).each do |node| 
  		    	event = website.events.build
  		     	#タイトルを取得
  		     	event.title = node.xpath(website.title_xpath).inner_text
    
  		     	# 各リンクを取得
  		     	event.url = node.xpath(website.link_xpath).attribute('href').value
    
  		     	# 日付、時間の取得
  		     	node.xpath(website.start_datetime_xpath).present? ? event.start_datetime = node.xpath(website.start_datetime_xpath).attribute("datetime").value : event.start_datetime = ""
  
  		     	#アドレスを表示	
  		     	event.address = node.xpath(website.address_xpath).inner_text
  
  		     	events << event unless existing_urls.include?(event.url)
  
  		    end #eachのend
  		  end
  
  		  # 続きのページがあるか見る
  		  if doc.xpath(list_day_xpath).count == 1 && doc.xpath(website.next_page_xpath).present?
  			  url = doc.xpath(website.next_page_xpath).attribute('href').value
  			else
  		  	loop = false
        end
		  end #whilのend

      when "https://connpass.com"
        events_array = []
        url = website.event_url + scraping_date.strftime("%Y%m")
        doc = open_url(url)
          #containsから返された複数あるeventlistを.eachで回す
        doc.xpath(website.content_xpath).each do |cp|
          cp.xpath(".//td").each do |td|
            unless td.xpath("div[@class='events']/ul/li").blank?
              link = td.xpath(".//div[@class='events']/ul/li[1]/a").attribute("href").value
              doc = open_url(link)
              date = Time.parse(doc.xpath(".//div[@id='side_area']//span[@class='dtstart']/span[@class='value-title']").attribute("title").value) + 9.hours
              date = date.to_date
              if date == scraping_date
                events_array = td.xpath(".//div[@class='events']/ul/li")
                break
              end
            end
          end
          unless events_array.blank?
            break
          end
        end
    
        unless events_array.blank?
          events_array.each do |node| 
            event = website.events.build
            #タイトルを取得
            event.title = node.xpath(website.title_xpath).inner_text
        
            # 各リンクを取得
            link = node.xpath(website.link_xpath).attribute('href').value
      
            event.url = link
      
            dtail_doc = open_url(link)
        
            # 日付、時間の取得
            event.start_datetime = Time.parse(dtail_doc.xpath(".//div[@id='side_area']//span[@class='dtstart']/span[@class='value-title']").attribute("title").value)
      
            #場所を取得
            if dtail_doc.xpath(".//div[@id='side_area']//div[@class='group_inner event_place_area']/p[@class='adr']/span[@class='value-title']").blank?
              event.address = "オンライン"
            else
              event.address = dtail_doc.xpath(".//div[@id='side_area']//div[@class='group_inner event_place_area']/p[@class='adr']/span[@class='value-title']").attribute("title").value
            end
            events << event unless existing_urls.include?(event.url)
          end #eachのend
        end

   	end #caseのend

    Event.import events

	end

	def open_url(url)
		charset = nil
		html = open(url) do |f|
   		  	charset = f.charset # 文字種別を取得
   		  	f.read # htmlを読み込んで変数htmlに渡す
    end
   	# htmlをパース(解析)してオブジェクトを生成
    Nokogiri::HTML.parse(html, nil, charset)
	end

end
