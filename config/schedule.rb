# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#

# 各環境の頭からこのアプリケーションまでのパス(Rails.root)
require File.expand_path(File.dirname(__FILE__) + "/environment")

# cronを実行する環境変数
# 本番環境にあげる場合はrailsに合わせる or 本番環境だけにする
set :environment, :development

# cronのログを吐き出す場所
set :output, "#{Rails.root}/log/cron.log"

#
 every 1.day, at: ['14:20'] do
   rake "scraping:scrape"
 end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
