# URLにアクセスするためのライブラリの読み込み
require 'open-uri'

class EventsController < ApplicationController

	def index
		result = {"events": nil}
		date = Date.parse(params[:date]) #string型からdate型に変換
		result["events"] = Event.where("start_datetime >= ? and start_datetime < ?", date, (date + 1))
		render json: result
	end

end
